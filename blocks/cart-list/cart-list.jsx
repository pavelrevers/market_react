/** @jsx React.DOM */

var React = require('react');
var CartListItem = require('../../components/cart-list-item/cart-list-item.js');

var CartList = React.createClass({
    render: function () {
        console.log(this.props.offers);
        var cards = this.props.offers.map(function (offer) {
            console.log(offer);
            return <CartListItem offer={offer} />;
        });
        return (
            <ul className="cart-list">
                {cards}
            </ul>
        );
    }
});

module.exports = CartList;