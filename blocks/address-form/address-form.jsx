/** @jsx React.DOM */
var AddressForm = React.createClass({
	getInitialState: function () {
		return {
			street: this.props.address.get('street'),
			city: this.props.address.get('city')
		};
	},
	handleSubmit: function (e) {
		this.props.address.set({
			city: this.refs.city.getDOMNode().value,
			street: this.refs.street.getDOMNode().value
		});
    	return false;
  	},
  	handleChange: function (e) {
  		this.state[e.target.name] = e.target.value;
  		this.setState(this.state);
  	},
	render: function () {
        return (
        	<form className="address-form" onSubmit={this.handleSubmit}>
        		<input onChange={this.handleChange} ref="city" name="city" value={this.state.city} />
        		<input onChange={this.handleChange} ref="street" name="street" value={this.state.street} />
        		<input type="submit" value="Post" />
    		</form>
    	);
    }
});
