/** @jsx React.DOM */
var CheckoutOrders = React.createClass({
	render: function(){
		var totalSum = this.props.offers.val().reduce(function (prev, cur) {
            return prev.buyerTotalAmount + cur.buyerTotalAmount;
        }, {buyerTotalAmount: 0});
        return (
        	<div className="layout__col checkout-orders__col checkout-orders__col_type_right checkout-list__wrapper">
                <CheckoutOffers offers={this.props.offers} />
                <div className="checkout-list-summary">
                    Итого: <strong>{totalSum} руб.</strong>
                    <span className="js-checkout__delete-shop checkout-delete checkout-delete_type_active"></span>
                </div>
            </div>
    	);
    }
});
