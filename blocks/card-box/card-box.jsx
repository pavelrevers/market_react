/** @jsx React.DOM */

var React = require('react');
var CartList = require('../../components/cart-list/cart-list.js');

var CartBox = React.createClass({
    render: function () {
        var total = this.props.offers.reduce(function (total, offer) {
            return {
                sum: total.sum + offer.get('price') * offer.get('count'),
                count: total.count + offer.get('count')
            };
        }, {sum: 0, count: 0});
        return (
            <div className="js-checkout layout layout_type_market global__border-box">
                <div className="cart-box layout layout_type_market global__border-box">
                    <div className="cart-box__content">
                        <div className="island">
                            <div className="island__header layout layout_display_table">
                                <h1 className="cart-box__header layout__col title">Корзина</h1>
                                <div className="cart-box__caption layout__col">Количество</div>
                                <div className="cart-box__caption layout__col">Цена</div>
                            </div>
                            <CartList offers={this.props.offers} />
                            <form action="/checkout" method="post" className="js-cart-box__form cart-box__footer">
                                <div className="island__footer layout layout_display_table">
                                    <div className="cart-box__footer-inner layout__col">
                                        Итого: <strong>{total.count} товара</strong> на сумму <strong>{total.sum} руб.</strong>
                                    </div>
                                    <div className="cart-box__action layout__col">
                                        <button type="submit" role="button" className="button button_size_m button_theme_action i-bem js-mx-auth cart-box__submit button_js_inited">
                                        <span className="button__text">Оформить заказ</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = CartBox;