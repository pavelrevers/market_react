/** @jsx React.DOM */
var CheckoutOffer = React.createClass({
    render: function () {
        var offer = this.props.offer;
        
        var imgSrc = offer.pictures.val().filter(function (pic) {
            return pic.containerHeight == 50 && pic.containerWidth == 50;
        })[0].url;

        return (
            <li className="js-checkout__offer checkout-list__item">
                <div className="checkout-list__image">
                    <img className="image" src={imgSrc} alt={offer.offerName.val()} title={offer.offerName.val()} />
                </div>
                <div className="checkout-list__content">
                    <div className="checkout-list__category"></div>
                    <div className="checkout-list__name">{offer.offerName.val()}</div>
                    <div className="checkout-list__price">{offer.buyerTotalAmount.val()} руб.</div>
                    <div className="checkout-list__amount">{offer.count.val()} шт. × {offer.buyerPrice.val()} руб.</div>
                </div>
            </li>
      	);
    }
});
