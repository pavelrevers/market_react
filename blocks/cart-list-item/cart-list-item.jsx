/** @jsx React.DOM */

var React = require('react');
var CartCardAmount = require('../../components/cart-card-amount/cart-card-amount.js');

var CartListItem = React.createClass({
    deleteCard: function () {
        var offers = this.props.offer.collection;
        this.props.offer.destroy();
        offers.trigger('change');
    },
    render: function () {
        console.log(this.props.offer);
        var offerModel = this.props.offer;
        var offer = offerModel.toJSON();
        return (
            <li className="cart-list__item cart-card preloadable preloadable_theme_gray-32">
                <span className="cart-card__image">
                    <img alt="" src="http://0.cs-ellpic.yandex.net/market_1fXkzdQk8T2XXo8Er9vKrQ_100x100.jpg" width="47" height="100" className="image" />
                </span>
                <div className="cart-card__content">
                    <h4 className="cart-card__header">
                        <a href="/model.xml?modelid=10495456&amp;hid=91491&amp;track=cart" className="b-link">{offer.name}</a>
                    </h4>
                    <div className="cart-card__description">{offer.description}</div>
                </div>
                <CartCardAmount model={offerModel} count={offerModel.get('count')} min={1} max={999} />
                <div className="cart-card__info">
                    <strong className="cart-card__price">{offer.price} {offer.currency}</strong>
                    <div className="cart-card__onstock">{offer.onStock ? 'в наличии' : 'на заказ'}</div>
                    <span className="cart-card__details">Бесплатная доставка, возможен&nbsp;самовывоз</span>
                </div>
                <ul className="cart-card__toolbar">
                    <li className="cart-card__toolbar-item">
                        <span className="b-link_theme_green">{offer.shopName}</span>
                    </li>
                    <li className="cart-card__toolbar-item">
                        <div className="rating-stars" data-rate={offer.qualityRating}>
                            <i className="rating-stars__star"></i>
                            <i className="rating-stars__star"></i>
                            <i className="rating-stars__star"></i>
                            <i className="rating-stars__star"></i>
                            <i className="rating-stars__star"></i>
                        </div>
                    </li>
                </ul>
                <div className="cart-card__action">
                    <span className="b-link b-link_pseudo_yes b-link_action_delete b-link_theme_grey b-link_inner_yes cart-card__delete" onClick={this.deleteCard}>
                        <i className="b-link__icon"></i>
                        <span className="b-link__inner">Удалить</span>
                    </span>
                </div>
            </li>
        );
    }
});

module.exports = CartListItem;