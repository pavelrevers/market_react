/** @jsx React.DOM */
var CheckoutShop = React.createClass({
    getCurrentDeliveryOption: function () {

    },
	render: function(){
        return (
        	<div className="js-checkout__shop island layout layout_display_table checkout-orders">
        		<div className="layout__col checkout-orders__col checkout-orders__col_type_left">
					<div className="island__header">
                        <h2 className="title">Заказ в магазине </h2>
                    </div>
                    <div className="checkout-orders__content">
                        <CheckoutDelivery
                            deliveryOptions={this.props.shop.deliveryOptions}
                            deliveryOptionsTypes={this.props.shop.deliveryOptionsTypes}
                            shop={this.props.shop}
                            data={this.props.data} />
                    </div>
        		</div>
                <CheckoutOrders offers={this.props.shop.items} />
    		</div>
    	);
    }
});

var CheckoutDeliveryOption = React.createClass({
    handleClick: function (e) {
        console.time('render');
        this.props.data.request.ordersDeliveryData[this.props.shop.shopId.val()].deliveryType.set(this.props.option.val());
    },
    render: function () {
        console.timeEnd('render');
        var option = this.props.option.val();
        console.log(this.props.data.i18n, this.props.data.i18n.delivery)
        var deliveryOptionName = this.props.data.i18n.delivery[option].val();
        var currentDeliveryOption = this.props.data.request.ordersDeliveryData[this.props.shop.shopId.val()];

        return (
            <li onClick={this.handleClick} className={currentDeliveryOption.deliveryType.val() == option ? 'tabs__item tabs__item_state_selected js-checkout-delivery__type-active' : 'tabs__item'}>
                <span className="link js-checkout-delivery__type">{deliveryOptionName}</span>
            </li>
        );
    }
});
