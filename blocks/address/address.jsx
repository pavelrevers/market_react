/** @jsx React.DOM */
var Address = React.createClass({
	componentDidMount: function () {
		this.props.address.on('change', function () {
			this.state.edit = false;
			this.forceUpdate();
		}.bind(this));
	},
	getInitialState: function () {
		return {
			edit: false
		}
	},
	edit: function () {
		this.state.edit = true;
		this.setState(this.state);
	},
	render: function(){
		var edit;
		if (this.state.edit) {
			edit = <AddressForm address={this.props.address} />
		} else {
			edit = <button onClick={this.edit}>Edit</button>
		}
		return (
        	<div className="address">
        		<div className="address__text">{this.props.address.get('street')}</div>
        		<div className="address__text">{this.props.address.get('city')}</div>
        		{edit}
    		</div>
    	);
    }
});
