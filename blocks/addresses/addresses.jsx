/** @jsx React.DOM */
var Addresses = React.createClass({
	componentDidMount: function () {
		this.props.addresses.on('change', function () {
			this.forceUpdate()
		}.bind(this));
	},
	render: function() {
		var addresses = this.props.addresses.map(function (address) {
			return (
				<Address address={address} />
			)
		});
        return (
        	<div className="addresses">
        		{addresses}
    		</div>
    	);
    }
});
