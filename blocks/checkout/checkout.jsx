/** @jsx React.DOM */
var Checkout = React.createClass({
	render: function () {
        console.log(this.props.data);
		var carts = this.props.data.cart.data.carts.map(function (shop) {
            return <CheckoutShop shop={shop} data={this.props.data} />
		}.bind(this));
        return (
        	<div className="w-checkout">
        		<form className="js-checkout__widget">
					<div className="w-checkout__header">
						<h1 className="title">Оформление заказа</h1>
					</div>
        		</form>
                {carts}
    		</div>
    	);
    }
});
