/** @jsx React.DOM */
var CheckoutDeliveryType = React.createClass({
    handleClick: function (e) {
        console.time('render');
        this.props.data.request.ordersDeliveryData[this.props.shop.shopId.val()].deliveryType.set(this.props.option.val());
    },
    render: function () {
        
        return (
            <div className="tabs-content__item tabs-content__item_state_visible">
                <div>
                    <label className="radiobox__radio radiobox__radio_checked_yes" for="checkout-delivery-options-18468-vujQrQNMAdO8IAy7AZhvT0mcD6ZDVjbRRq2ms9zQ4dGAen6MYGcVU0Q4evyLIa1gUbBetTC2IsD2bpdBWN3tYNYsJ8QXVTZeKbtePiwTCcTeK2t8LTQvVVOtb/xHMAAw">
                        <span className="radiobox__box">
                            <input className="js-checkout__delivery radiobox__control" value="vujQrQNMAdO8IAy7AZhvT0mcD6ZDVjbRRq2ms9zQ4dGAen6MYGcVU0Q4evyLIa1gUbBetTC2IsD2bpdBWN3tYNYsJ8QXVTZeKbtePiwTCcTeK2t8LTQvVVOtb/xHMAAw" id="checkout-delivery-options-18468-vujQrQNMAdO8IAy7AZhvT0mcD6ZDVjbRRq2ms9zQ4dGAen6MYGcVU0Q4evyLIa1gUbBetTC2IsD2bpdBWN3tYNYsJ8QXVTZeKbtePiwTCcTeK2t8LTQvVVOtb/xHMAAw" name="checkout-delivery__items-18468" type="radio" checked="checked"/>
                        </span>
                        Собственная служба доставки&nbsp;&nbsp;•&nbsp;&nbsp;22 – 23 июля&nbsp;&nbsp;•&nbsp;&nbsp;бесплатно
                    </label>
                </div>
            </div>
        );
    }
});


