/** @jsx React.DOM */

var React = require('react');

var CartCardAmount = React.createClass({
    MIN_COUNT: 1,
    MAX_COUNT: 999,
    decreaseCount: function () {
        this.props.model.set('count', this.props.count - 1);
    },
    increaseCount: function () {
        this.props.model.set('count', this.props.count + 1);
    },
    changeCount: function (e) {
        var count = e.target.value - 0;
        if (!isNaN(count)) {
            if (count < this.MIN_COUNT) count = this.MIN_COUNT;
            if (count > this.MAX_COUNT) count = this.MAX_COUNT;
            this.props.model.set('count', count);
        }
    },
    getInitialState: function () {
        return {
            minCountReached: this.props.count == this.MIN_COUNT,
            maxCountReached: this.props.count == this.MAX_COUNT,
        };
    },
    componentWillReceiveProps: function (nextProps) {
        this.setState({
            minCountReached: nextProps.count == this.MIN_COUNT,
            maxCountReached: nextProps.count == this.MAX_COUNT,
        });
    },
    shouldComponentUpdate: function(nextProps, nextState) {
        return nextProps.count !== this.props.count;
    },
    render: function () {
        var count = this.props.count;
        var decreaseButtonAction = this.decreaseCount;
        var increaseButtonAction = this.increaseCount;
        var decreaseCountButtonClassName = "button button_size_s button_theme_normal";
        var increaseCountButtonClassName = "button button_size_s button_theme_normal";
        if (this.state.minCountReached) {
            decreaseCountButtonClassName +=" button_disabled_yes";
            decreaseButtonAction = null;
        } else if (this.state.maxCountReached) {
            increaseCountButtonClassName += " button_disabled_yes";
            increaseButtonAction = null;
        }

        return (
            <div className="cart-card__amount">
                <button onClick={decreaseButtonAction} type="button" role="button" className={decreaseCountButtonClassName}>
                    <span className="button__text">–</span>
                </button>
                <span className="input input_size_s input_clear_no input_theme_normal cart-card__amount-input i-bem input_js_inited" onclick="return {'input':{}}">
                    <span className="input__box">
                        <input onChange={this.changeCount} value={count} className="input__control" />
                    </span>
                    <span className="input__query-holder"></span>
                    <span className="input__query-holder">1</span>
                </span>
                <button onClick={increaseButtonAction} type="button" role="button" className={increaseCountButtonClassName}>
                    <span className="button__text">+</span>
                </button>
            </div>
        );
    }
});

module.exports = CartCardAmount;