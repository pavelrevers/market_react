/** @jsx React.DOM */
var Header = React.createClass({displayName: 'header',
	render: function(){
        return (
        	<div className="header">
        		<header__logo />
        		<Input />
        		<Addresses />
    		</div>
    	);
    }
});
