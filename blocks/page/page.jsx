/** @jsx React.DOM */
var React = require('react');

var Page = React.createClass({
    render: function () {
        return (
            <html class="i-ua_js_yes i-ua_css_standard  i-ua_svg_yes i-ua_inlinesvg_yes i-ua_svg_yes i-ua_inlinesvg_yes i-ua_svg_yes i-ua_inlinesvg_yes">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                    <title>React тест</title>
                    <link rel="stylesheet" type="text/css" href="static/main.css" />
                </head>
                <body className="b-page">
                    <div className="b-max-width">
                        {this.props.children}
                    </div>
                    <script src="static/vendor.bundle.js"></script>
                    <script src="static/bundle.js"></script>
                </body>
            </html>
        );
    }
});

module.exports = Page;