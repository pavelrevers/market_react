/** @jsx React.DOM */
var CheckoutDelivery = React.createClass({
    render: function(){
		var deliveryOptionsTypes = this.props.deliveryOptionsTypes.map(function (option, index) {
			return (
				<CheckoutDeliveryOption
                    option={option}
                    shop={this.props.shop}
                    data={this.props.data} />
			)
		}.bind(this));
        var deliveryOptions = this.props.deliveryOptionsTypes.map(function (option, index) {
            return (
                <CheckoutDeliveryType
                    option={option}
                    shop={this.props.shop}
                    data={this.props.data} />
            )
        }.bind(this));
        return (
        	<div className="checkout-delivery tabs-content js-checkout__set-default-radio">
                <div className="checkout-delivery__header">
                    <ul className="tabs tabs_interactive_true">
                        {deliveryOptionsTypes}
                    </ul>
                </div>
                <span className="radiobox radiobox_size_s radiobox_theme_normal i-bem radiobox_js_inited" onclick="return {&quot;radiobox&quot;:{}}">
                {deliveryOptions}
                <div className="tabs-content__item tabs-content__item_state_visible"><div>
                    <label className="radiobox__radio radiobox__radio_checked_yes" for="checkout-delivery-options-18468-vujQrQNMAdO8IAy7AZhvT0mcD6ZDVjbRRq2ms9zQ4dGAen6MYGcVU0Q4evyLIa1gUbBetTC2IsD2bpdBWN3tYNYsJ8QXVTZeKbtePiwTCcTeK2t8LTQvVVOtb/xHMAAw">
                        <span className="radiobox__box">
                            <input className="js-checkout__delivery radiobox__control" value="vujQrQNMAdO8IAy7AZhvT0mcD6ZDVjbRRq2ms9zQ4dGAen6MYGcVU0Q4evyLIa1gUbBetTC2IsD2bpdBWN3tYNYsJ8QXVTZeKbtePiwTCcTeK2t8LTQvVVOtb/xHMAAw" id="checkout-delivery-options-18468-vujQrQNMAdO8IAy7AZhvT0mcD6ZDVjbRRq2ms9zQ4dGAen6MYGcVU0Q4evyLIa1gUbBetTC2IsD2bpdBWN3tYNYsJ8QXVTZeKbtePiwTCcTeK2t8LTQvVVOtb/xHMAAw" name="checkout-delivery__items-18468" type="radio" checked="checked"/>
                        </span>
                        Собственная служба доставки&nbsp;&nbsp;•&nbsp;&nbsp;22 – 23 июля&nbsp;&nbsp;•&nbsp;&nbsp;бесплатно
                    </label>
                </div>
                </div>
                <div className="tabs-content__item">
                <div className="w-checkout-delivery__outlets">Вы можете забрать&nbsp;товары в&nbsp;пункте&nbsp;выдачи «"Десятое измерение" на Бауманской»</div>
                </div>
                </span>
                </div>
    	);
    }
});
