/** @jsx React.DOM */
var CheckoutOffers = React.createClass({
    render: function(){
		var offers = this.props.offers.map(function (offer) {
            return (
                <CheckoutOffer offer={offer} />
            )
        });
        return (
        	<ul className="checkout-list">
                {offers}
            </ul>
	    );
    }
});
