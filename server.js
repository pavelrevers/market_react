var express = require('express');
var app = express();
var React = require('react');
var Page = require('./components/page/page.js');

app.use('/static', express.static(__dirname + '/public'));
app.use('/static', express.static(__dirname + '/'));

app.get('/', function(req, res){
    res.send('<!DOCTYPE html>' + React.renderComponentToString(Page()));
});

var server = app.listen(3000, function() {
    console.log('Listening on port %d', server.address().port);
});
