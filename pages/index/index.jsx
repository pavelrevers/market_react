/** @jsx React.DOM */
var Backbone = require('backbone');
var React = require('react');
var CartBox = require('../../components/card-box/card-box.js');
var Cart = require('../../app/collections/Cart.js');
var Page = require('../../components/page/page.js');

var data = [{
    "id":"-xSuqLY0z41MaYWZ0WqIgA",
    "name":"Мобильный телефон HTC One 32Gb Silver",
    "type":"OFFER",
    "shopId":704,
    "modelId":9281443,
    "hid":91491,
    "cpa":"real",
    "urlCpa":"/redir/338FT8NBgRt8rDPhjkWNjaONTiBLHcS37Z-VS1SS63nGZXNawm8_7olTZMd0t7eddPbq67cTcjMBBdAHRfLOUkR8DrJ2mVolE6svRqHHLEFjCaHCPE5N67DuCaMof4Hkq6bfNGGeKowyk33rIeb-4A2s76yt2zTXsIe74YQ3wZbtDQ1nxz4OK2pXoVbrwzg9inqQMl8WfO84HLnJoy2MIXmIEzy8ijab10Yb5nCb-hKM5nVVLDmYLYz2muV1Jqpdj1CkwuYoS6lUuyxX1Zg1z4QlqKUq550u_AvrOSwf1rbhRSGF09bTHz2-TSgn6gwbUqleYnNIuhQUCGl1Ij_3VRNc4Y0kVQDd0utVrr2HNv9DFqPh8oJ5AA7HbWV1TGPmjAv9Aii5eBKDOxRjZcwnbDYSM_8aTTHcfAwOuAKAtjk554uWorufNzpaaj6VhwavCLZU3MU37Wine-hHALGPzsFENT6giBTsWIP2OBerJ0VPzyGJG9Qn3YgIRdDmPJf4qZV227tlSG8?data=QVyKqSPyGQwwaFPWqjjgNsPUXfxrTruin3rZLX0L6CPPYaHJ21Q41jHOnp4fRPiGWUnKyFZ_6Ak_kUM7y1JL3IKKt1QGKhmWgUYAL3r9UmRbMCn39CrA7ezMp1Vm4v1mqZGGqnes0AGUQRkeYavAhL3y9Nc_5prfobkgNSV42UyPbWJnTVZ5o10xYsvxkgh1jPF5LpxHMFQ9-hWAmkEYw7FiRqSPaMVWfcZhbT2mBz7JEU_bmY5TJ0Ru6Qj_5_DtKnAURBIsxriL2xBClyVKXElHAxkIUBOb5Gwy9bHj-YSHumcOisd9p7wZSEtel8mnm9PfrYWOW6J7QPElxBnIQGut1Yx2wAM-WMN3WlwwQGwnlrBKAvypoBiXCpidoclQcv6VI7xp4kISjUAB0vdouBDHd1wrqCmNvLXMLKwHvVP9RD1DESPpAHw1spxZcaEDQBg8v0S5BwXdv6_6W5ej-Xtk8yxlTkd1y9IxXE0Gsebz2HUa4igzIQ08q60wD-QSP9CbNwbv2Eo&b64e=2&sign=01486d0e1e6e420cc16c9e66bb235e92&keyno=1","cpc":"Ls-JZc-MtPvvEAD6aaHbZ-pgKrgk7eQJaWtyhHgGK1X4we2E0dkCtQ",
    "onStock":true,
    "fee":"4zINsNPJIb55qJqPDPii42ZIckPZQVRJBPZsd-XW5WLKw2A2ZU9-32o02HiQRkAcDaQZ0kKNNp4",
    "price":20990,
    "count":1,
    "feedId":1091,
    "currency":"RUR",
    "description":"+ Подарок! HTC CAR D170 для HTC One mini Держатель автомобильный  с автозарякой micro-USB",
    "offerId":"313549",
    "manufacturerWarranty":"",
    "sellerWarranty":"",
    "images":[{
        "0":{
            "url":"http://0.cs-ellpic.yandex.net/market_4e-aZfMGaq9bDMwPONV3EA_90x120.jpg",
            "width":64,
            "height":120
        },
        "1":{
            "url":"http://0.cs-ellpic.yandex.net/market_4e-aZfMGaq9bDMwPONV3EA_190x250.jpg",
            "width":133,
            "height":250
        },
        "2":{
            "url":"http://0.cs-ellpic.yandex.net/market_4e-aZfMGaq9bDMwPONV3EA_74x100.jpg",
            "width":53,
            "height":100
        },
        "3":{
            "url":"http://0.cs-ellpic.yandex.net/market_4e-aZfMGaq9bDMwPONV3EA_60x80.jpg",
            "width":42,
            "height":80
        },"4":{"url":"http://0.cs-ellpic.yandex.net/market_4e-aZfMGaq9bDMwPONV3EA_100x100.jpg","width":53,"height":100},"5":{"url":"http://0.cs-ellpic.yandex.net/market_4e-aZfMGaq9bDMwPONV3EA_300x300.jpg","width":159,"height":300},"6":{"url":"http://0.cs-ellpic.yandex.net/market_4e-aZfMGaq9bDMwPONV3EA_150x200.jpg","width":106,"height":200},"7":{"url":"http://0.cs-ellpic.yandex.net/market_4e-aZfMGaq9bDMwPONV3EA_50x50.jpg","width":26,"height":50},"8":{"url":"http://0.cs-ellpic.yandex.net/market_4e-aZfMGaq9bDMwPONV3EA_55x70.jpg","width":37,"height":70},"9":{"url":"http://0.cs-ellpic.yandex.net/market_4e-aZfMGaq9bDMwPONV3EA_180x240.jpg","width":128,"height":240},"10":{"url":"http://0.cs-ellpic.yandex.net/market_4e-aZfMGaq9bDMwPONV3EA_110x150.jpg","width":80,"height":150},
        "id":null,
        "length":11,
        "map":["90120","190250","74100","6080","100100","300300","150200","5050","5570","180240","110150"]
    }],
    "delivery":{
        "regionID":213,
        "shipping":true,
        "shippingLong":"По Москве и за МКАД до 15 км. 350 руб. курьером, вес заказа до 10 кг, 700 руб - автомобилем, заказы весом свыше 10 кг.",
        "pickup":true,
        "store":true,
        "free":false,
        "downloadable":false,
        "price":390,
        "priority":true,
        "priorityRegionID":213,
        "priorityRegionName":"Москва"
    },
    "shopName":"Xcom-Shop",
    "qualityRating":5
}, {
    "id":"U6RG-PVjwi67lDibxbFKfQ","name":"Apple iPhone 5S 16GB Space Gray","type":"OFFER","shopId":1613,"modelId":10495456,"hid":91491,"cpa":"real","urlCpa":"/redir/338FT8NBgRt8rDPhjkWNjaONTiBLHcS3pzT0FQr19rcb9L70E8HzB3_Fd7bMAUpiC5HjuLDnvBJVenzcC0k1lEZK2dCo2xj_G0j8230Zyta_3rKAycBeuJvzOy242OLJOBYrAqWpmZNk9a-Fbe_LrQLqY1cL2PVo3OXruMBESgieCHluQnec3WDIsEL41pMEQUQ_vdyUDXNC08GaF4n3fjwitA9vXyc1-8IXimHuZnlKclOeEK6EnnJWJrxNCgml7kri0lo0SxCews5oTvijWTXHz9LVLpYC1miUR1gHHeQptvLYEMDeCcckroN1EMDP20JLmR5yuBn59DywOlMeoPq5nqDUO0v_x_DApYM9ez7dw0jSaLsotn00kl_-3UaQEh8p0kbUoajFM5YnMSleLSQKF7upjiLsabZ2W8tOpiUoGOXEvehM6vHOYhNfyEYyV3QKcQUWTNE4MCfz1PrvQV0WXVg9_lAxinvn0WR4vTZkGERZhE_sUPU9xzHu3ge2GK8iy_UouRM?data=QVyKqSPyGQwwaFPWqjjgNp9Am0bj41xax0O-GGLilcgBP2K9GELQZgDIoZUgkIK2YtNfVIXOBNn57ErKGlA3jSJ42bfmvA3px0I3QX1ouJuZ1rqpNow_kPc5yxZ_LoHIMXSqCN-o01XN6G7VsbDLM5CsfND1yGjKYxIH-iJ9SdyqQnDi2Zp4I9tKxxw2DvyMchX4_CT8qM63yagiyMVZABJEPo9CaeKOLpi08njtgQnbOpXqGUYaOnDxuOiknZw_G2NjnY2eJEYS4ImQRQ-LbdbuHa-5n-OHJCMgViiQByujhRCDNNR2n17CBCBPILpxqshRt-1JKqTWriKSPd5M3p0nXm-Q7hFrdh1bIjeyfK-uyCbb7vWjr2T11X33h9I1J-OvfDIXeFH4JFV48Lp8P46g3CYT6erKLn32W3dCjd2BiDlDP7YvoNLjIT21_Tzyzip8PqOnx8WrQOmeMF33PLmyANzes9nW&b64e=2&sign=38ba029ca7640f9c2f78fc77b9c77dae&keyno=1","cpc":"DZx9BZzRfJVd-9H81ql5cBcsWAgp_pfAPvhG9SzPXKf32uCHVWwZ6A","onStock":false,"fee":"4zINsNPJIb7qUw2-QoOSnSod2u-yTlp4qlrRnju9UxUCrh6EnECr5pRxWPH5xvkrBJwgMZQYP4Y","price":28990,"count":1,"feedId":2464,"currency":"RUR","description":"Компания Apple представила новый флагманский смартфон iPhone 5S. Смартфон снабжен новым процессором А7, он работает в два раза быстрее предыдущей модели. Емкость батареи новой модели рассчитана на 10 часов работы в высокоскоростных сетях. Диагональ экрана составляет 4 дюйма, разрешение - 1136 на 640 пикселей.","offerId":"910525o2836717","manufacturerWarranty":"","sellerWarranty":"","images":[{"0":{"url":"http://0.cs-ellpic.yandex.net/market_1fXkzdQk8T2XXo8Er9vKrQ_90x120.jpg","width":57,"height":120},"1":{"url":"http://0.cs-ellpic.yandex.net/market_1fXkzdQk8T2XXo8Er9vKrQ_74x100.jpg","width":48,"height":100},"2":{"url":"http://0.cs-ellpic.yandex.net/market_1fXkzdQk8T2XXo8Er9vKrQ_60x80.jpg","width":38,"height":80},"3":{"url":"http://0.cs-ellpic.yandex.net/market_1fXkzdQk8T2XXo8Er9vKrQ_100x100.jpg","width":47,"height":100},"4":{"url":"http://0.cs-ellpic.yandex.net/market_1fXkzdQk8T2XXo8Er9vKrQ_150x200.jpg","width":95,"height":200},"5":{"url":"http://0.cs-ellpic.yandex.net/market_1fXkzdQk8T2XXo8Er9vKrQ_50x50.jpg","width":24,"height":50},"6":{"url":"http://0.cs-ellpic.yandex.net/market_1fXkzdQk8T2XXo8Er9vKrQ_55x70.jpg","width":33,"height":70},"7":{"url":"http://0.cs-ellpic.yandex.net/market_1fXkzdQk8T2XXo8Er9vKrQ_180x240.jpg","width":114,"height":240},"8":{"url":"http://0.cs-ellpic.yandex.net/market_1fXkzdQk8T2XXo8Er9vKrQ_110x150.jpg","width":71,"height":150},"id":null,"length":9,"map":["90120","74100","6080","100100","150200","5050","5570","180240","110150"]},{"0":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_90x120.jpg","width":57,"height":120},"1":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_110x150.jpg","width":71,"height":150},"2":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_100x100.jpg","width":47,"height":100},"3":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_74x100.jpg","width":47,"height":100},"4":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_60x80.jpg","width":38,"height":80},"5":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_300x300.jpg","width":142,"height":300},"6":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_900x1200.jpg","width":571,"height":1200},"7":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_600x600.jpg","width":285,"height":600},"8":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_190x250.jpg","width":119,"height":250},"9":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_150x200.jpg","width":95,"height":200},"10":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_300x400.jpg","width":190,"height":400},"11":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_50x50.jpg","width":23,"height":50},"12":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_55x70.jpg","width":33,"height":70},"13":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_600x800.jpg","width":380,"height":800},"14":{"url":"http://1.cs-ellpic.yandex.net/market_5vqGoaNJizGiMrYYUWWwQw_180x240.jpg","width":114,"height":240},"id":null,"length":15,"map":["90120","110150","100100","74100","6080","300300","9001200","600600","190250","150200","300400","5050","5570","600800","180240"]}],"delivery":{"regionID":213,"shipping":true,"shippingLong":"Самовывоз - от 990 руб. бесплатно<br>Курьером (от 1990 руб. в пределах Москвы) - бесплатно<br>Курьером (до 1990 руб. в пределах Москвы) - 200 руб.<br>Курьером (за пределы Москвы) - уточнять у операторов","pickup":true,"store":true,"free":true,"downloadable":false,"price":0,"priority":true,"priorityRegionID":213,"priorityRegionName":"Москва"},"shopName":"Сотмаркет","qualityRating":4}, {"id":"N0QcuS2A1pS8xHTf1UFWag","name":"Мобильный телефон (смартфон) SONY Xperia Z2 (D6503) Purple","type":"OFFER","shopId":255,"modelId":10724878,"hid":91491,"cpa":"real","urlCpa":"/redir/338FT8NBgRt8rDPhjkWNjaONTiBLHcS3WhS2LLlMrj_HblKTXgF-xuvbmfRBJPofw6KuXTvM_uaumL4klfE6-Re4jJTUJwfh6DYPoaIB5KYxA7EmVdjMyteOAirdni-yCHg0r8vraOTqdJyIg1cHqUlZ-dh3GY-dYPtiT7aYe12hXLXsrPlgP_yI3gG10aTxFlP_NCQy8rhZ_ewbyDJZAVClqvCSsmEBYuoG3-fEyDgOJhxbo2PvdF74jyqXOSwP12_QUbj9wr6VvVF26n1pprleBxrx0Khkbt9b8NNkzXVxbTRIRl3enFU7cA1J4P3RKsLievQd1LcQDMreaBVL4MoClF_nPS8NqCWLOZRbtnehZ_wpOj8-qnBMtREr3_0C9oAonU2SjgcI7gBQhZKLtayDdmvmacW7U9VwhKnVh4DUmIsZVD_c9Q8_7MT1Mi_Al_iCpoY2DwphJjcTtAAhXuiE1xL3q5-QtKIutVgNdG0XVSn7S3QV6MX9p7vV4_5T9RNgLN3Ivp4?data=QVyKqSPyGQwwaFPWqjjgNuSvGGb_NyV9AhUu0zpNdUenyfAOmz3_-tYTbCiudvWMoxmGaSdX_dFfWZDY20rdTOA19Lckd_25yf13hDrtSc6NdcYciMS2vciPcoeofmaNPay1ucdjPxlhDuHGN5wKfeKEbKsNc8Cwduy6ERPjwvmywrxFBp9Bamo_9uWVG6Y7MnY7pHZGIU9N596joBF0VYFqQZUDXwgroqumqTJ2SF-x_uXjkRDOie_ThAcAEQ63TyNwJx2yoN_fpz5STsBCA-i1Dvqy9-MMlMtDY0uhKTdQkkrFwDg6p_dyRcEhPc33_Xja-83k9L6BmJMZlq21Ewag8fuNJ9aYSiKJrWm8ScC0ZtMzRsYCWwfN8X_Il_1xCM6x_C1jSHhVSP0-x80vfhFFg44mbFsbb6yxMIZwWUCEk6gS2HQ23XwXgzEEyw4Q&b64e=2&sign=2e84341fae7b5a9007d2bda8a8991f70&keyno=1","cpc":"zUVmr-0tvXBtwKjScIsJ2tPYrl2cgtLRTa1r6Jp3zgrnOqgN2mXhhg","onStock":true,"fee":"4zINsNPJIb6xYqekf4bUoYpuOLkeD4wk_ae1-enupxpUPpW6RFDuw5HpghKhs0EyQJpovlz11lk","price":29990,"count":1,"feedId":1526,"currency":"RUR","offerId":"91819","manufacturerWarranty":"","sellerWarranty":"","images":[{"0":{"url":"http://0.cs-ellpic.yandex.net/market_LhUI4Wt9pB0KKRSKlVeC1g_90x120.jpg","width":90,"height":90},"1":{"url":"http://0.cs-ellpic.yandex.net/market_LhUI4Wt9pB0KKRSKlVeC1g_190x250.jpg","width":190,"height":190},"2":{"url":"http://0.cs-ellpic.yandex.net/market_LhUI4Wt9pB0KKRSKlVeC1g_74x100.jpg","width":74,"height":74},"3":{"url":"http://0.cs-ellpic.yandex.net/market_LhUI4Wt9pB0KKRSKlVeC1g_60x80.jpg","width":60,"height":60},"4":{"url":"http://0.cs-ellpic.yandex.net/market_LhUI4Wt9pB0KKRSKlVeC1g_100x100.jpg","width":100,"height":100},"5":{"url":"http://0.cs-ellpic.yandex.net/market_LhUI4Wt9pB0KKRSKlVeC1g_300x300.jpg","width":300,"height":300},"6":{"url":"http://0.cs-ellpic.yandex.net/market_LhUI4Wt9pB0KKRSKlVeC1g_150x200.jpg","width":150,"height":150},"7":{"url":"http://0.cs-ellpic.yandex.net/market_LhUI4Wt9pB0KKRSKlVeC1g_300x400.jpg","width":300,"height":300},"8":{"url":"http://0.cs-ellpic.yandex.net/market_LhUI4Wt9pB0KKRSKlVeC1g_50x50.jpg","width":50,"height":50},"9":{"url":"http://0.cs-ellpic.yandex.net/market_LhUI4Wt9pB0KKRSKlVeC1g_55x70.jpg","width":55,"height":55},"10":{"url":"http://0.cs-ellpic.yandex.net/market_LhUI4Wt9pB0KKRSKlVeC1g_180x240.jpg","width":180,"height":180},"11":{"url":"http://0.cs-ellpic.yandex.net/market_LhUI4Wt9pB0KKRSKlVeC1g_110x150.jpg","width":110,"height":110},"id":null,"length":12,"map":["90120","190250","74100","6080","100100","300300","150200","300400","5050","5570","180240","110150"]},{"0":{"url":"http://1.cs-ellpic.yandex.net/market_Zsxi5Vlty14Tvneg3pRrZg_90x120.jpg","width":90,"height":90},"1":{"url":"http://1.cs-ellpic.yandex.net/market_Zsxi5Vlty14Tvneg3pRrZg_190x250.jpg","width":190,"height":190},"2":{"url":"http://1.cs-ellpic.yandex.net/market_Zsxi5Vlty14Tvneg3pRrZg_74x100.jpg","width":74,"height":74},"3":{"url":"http://1.cs-ellpic.yandex.net/market_Zsxi5Vlty14Tvneg3pRrZg_60x80.jpg","width":60,"height":60},"4":{"url":"http://1.cs-ellpic.yandex.net/market_Zsxi5Vlty14Tvneg3pRrZg_100x100.jpg","width":100,"height":100},"5":{"url":"http://1.cs-ellpic.yandex.net/market_Zsxi5Vlty14Tvneg3pRrZg_300x300.jpg","width":300,"height":300},"6":{"url":"http://1.cs-ellpic.yandex.net/market_Zsxi5Vlty14Tvneg3pRrZg_150x200.jpg","width":150,"height":150},"7":{"url":"http://1.cs-ellpic.yandex.net/market_Zsxi5Vlty14Tvneg3pRrZg_300x400.jpg","width":300,"height":300},"8":{"url":"http://1.cs-ellpic.yandex.net/market_Zsxi5Vlty14Tvneg3pRrZg_50x50.jpg","width":50,"height":50},"9":{"url":"http://1.cs-ellpic.yandex.net/market_Zsxi5Vlty14Tvneg3pRrZg_55x70.jpg","width":55,"height":55},"10":{"url":"http://1.cs-ellpic.yandex.net/market_Zsxi5Vlty14Tvneg3pRrZg_180x240.jpg","width":180,"height":180},"11":{"url":"http://1.cs-ellpic.yandex.net/market_Zsxi5Vlty14Tvneg3pRrZg_110x150.jpg","width":110,"height":110},"id":null,"length":12,"map":["90120","190250","74100","6080","100100","300300","150200","300400","5050","5570","180240","110150"]}],"delivery":{"regionID":213,"shipping":true,"shippingLong":"- бесплатный самовывоз! (17 магазинов)<br>- ускоренная доставка в пределах МКАД от 250 р., за МКAД до 40 км. от 400 р, <br>- доставка по России - по тарифам ТК!","pickup":true,"store":true,"free":true,"downloadable":false,"price":0,"priority":true,"priorityRegionID":213,"priorityRegionName":"Москва"},"shopName":"ОНЛАЙН ТРЕЙД.РУ","qualityRating":5}];

console.log('raw data:', JSON.stringify(data));

var offersData = localStorage.getItem('cart-list') ? JSON.parse(localStorage.getItem('cart-list')) : data;
var cart = new Cart(offersData);
console.log('Offers Collection:', cart);

var parent = document.querySelector('body>div');

var checkout = React.renderComponent((
	   <CartBox offers={cart} />
), parent);

cart.on('change', function () {
    console.log('change', arguments);
    localStorage.setItem('cart-list', JSON.stringify(cart));
    checkout.setProps({data: cart});
});

window.addEventListener('storage', function (e) {
    if (e.oldValue != e.newValue) {
        cart.set(JSON.parse(e.newValue));
        console.log('storage', arguments);
    }
});