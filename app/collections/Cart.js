var Backbone = require('backbone');
var Offer = require('../models/Offer.js');

var Cart = Backbone.Collection.extend({
    model: Offer
});

module.exports = Cart;