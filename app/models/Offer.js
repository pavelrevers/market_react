var Backbone = require('backbone');

var Offer = Backbone.Model.extend({
    defaults: {
        count: 0,
        cpa: "",
        cpc: "",
        currency: "",
        delivery: Object,
        description: "",
        fee: "",
        feedId: 0,
        hid: 0,
        id: "",
        images: Array[1],
        manufacturerWarranty: "",
        modelId: 0,
        name: "",
        offerId: "",
        onStock: false,
        price: 0,
        qualityRating: 0,
        sellerWarranty: "",
        shopId: 0,
        shopName: "",
        type: "",
        urlCpa: ""
    },
    sync: function () {

    }
});

module.exports = Offer;