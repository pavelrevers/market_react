var gulp = require('gulp');
var react = require('gulp-react');
var flatten = require('gulp-flatten');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var gulpWebpack = require('gulp-webpack');
var webpack = require('webpack');

gulp.task('default', ['react', 'index']);

gulp.task('react', function(){
    return gulp.src(['./blocks/**/*.jsx', './pages/**/*.jsx'])
        .pipe(react())
        .pipe(gulp.dest('./components'));
});

gulp.task('index', function () {
    return gulp.src('./components/index/index.js')
        .pipe(gulpWebpack({
            entry: {
                app: './components/index/index.js',
                vendor: ["backbone", "underscore", "react"],
            },
            output: {
                filename: "bundle.js"
            },
            plugins: [
                new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"vendor", /* filename= */"vendor.bundle.js")
            ]
        }))
        //.pipe(concat('union.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public'));
});